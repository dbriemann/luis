package main

import (
	"fmt"
	"html/template"

	"github.com/asdine/storm"
	"github.com/asdine/storm/codec/gob"
	"github.com/dbriemann/luis/utils"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

var (
	sessionKey string
	bossKey    string
	db         *storm.DB
)

func main() {
	s, err := utils.GenerateSecureKey(32)
	if err != nil {
		panic(err)
	}
	sessionKey = s

	b, err := utils.GenerateSecureKey(64)
	if err != nil {
		panic(err)
	}
	bossKey = b

	// HACK DEBUG
	bossKey = "debug"
	// HACK DEBUG

	// Open bolt db instance.
	instance, err := storm.Open("data/data.db", storm.Codec(gob.Codec))
	if err != nil {
		panic(err)
	}
	defer db.Close()

	db = instance

	// Router instance
	e := echo.New()

	// Middlewares
	e.Use(middleware.Gzip())
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// Static files
	e.Static("/public", "public")

	t := &Template{
		templates: template.Must(template.ParseGlob("templates/*.html")),
	}
	e.Renderer = t

	// Route => handler
	e.GET("/", rootView, Auth)
	e.GET("/login", loginView)
	e.POST("/login", loginPost)
	e.GET("/album/:id", albumView, Auth)
	e.POST("/visitors/add", visitorsAdd, Auth)
	e.POST("/visitors/del", visitorsDel, Auth)
	e.POST("/albums/add", albumsAdd, Auth)
	e.POST("/albums/del", albumsDel, Auth)
	e.POST("/link/:aid", linkSet, Auth)

	fmt.Println("***************")
	fmt.Println("*** BOSSKEY *** ==>", bossKey)
	fmt.Println("***************")

	// Start server
	e.Logger.Fatal(e.Start(":7777"))
}
