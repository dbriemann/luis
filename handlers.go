package main

import (
	"errors"
	"fmt"
	"net/http"
	"time"

	"crypto/subtle"

	"github.com/dbriemann/luis/utils"
	"github.com/labstack/echo"
)

func loginView(c echo.Context) error {
	return c.Render(http.StatusOK, "login.html", nil)
}

func loginPost(c echo.Context) error {
	bkey := c.FormValue("password")

	if subtle.ConstantTimeCompare([]byte(bkey), []byte(bossKey)) == 1 {
		// Logged in with boss key.
		// Create new session key.
		nsKey, err := utils.GenerateSecureKey(32)
		if err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, "Sorry.. something went wrong.")
		}

		sessionKey = nsKey

		// Set session key in cookie.
		cookie := new(http.Cookie)
		cookie.Name = "sessionkey"
		cookie.Value = sessionKey
		cookie.Expires = time.Now().Add(7 * 24 * time.Hour)
		c.SetCookie(cookie)

		// Redirect to main page.
		c.Redirect(http.StatusSeeOther, "/")
		return nil
	}

	return echo.NewHTTPError(http.StatusUnauthorized, "Authorization denied.")
}

func rootView(c echo.Context) error {
	// redirect to login page if user is not authed in any way
	boss, _ := c.Get("ISBOSS").(bool)
	uid, _ := c.Get("USERID").(string)
	if !boss && uid == "" {
		c.Redirect(http.StatusSeeOther, "/login")
		return nil
	}
	data := map[string]interface{}{
		"ISBOSS": boss,
		"USERID": uid,
		"TITLE":  "-- TODO --",
	}

	// query all visitors for listing
	var visitors []Visitor
	err := db.All(&visitors)
	if err != nil {
		data["ERRORMSG"] = "DB error: " + err.Error()
	} else {
		data["VISITORS"] = visitors
	}

	// query all albums for listing
	var albums []Album
	err = db.All(&albums)
	if err != nil {
		fmt.Println(err.Error())
		data["ERRORMSG"] = "DB error: " + err.Error()
	} else {
		fmt.Println("###", albums)
		data["ALBUMS"] = albums
	}

	return c.Render(http.StatusOK, "index.html", &data)
}

func albumView(c echo.Context) error {
	// redirect to login page if user is not authed in any way
	boss, _ := c.Get("ISBOSS").(bool)
	uid, _ := c.Get("USERID").(string)
	if !boss && uid == "" {
		c.Redirect(http.StatusSeeOther, "/login")
		return nil
	}
	data := map[string]interface{}{
		"ISBOSS": boss,
		"USERID": uid,
		"TITLE":  "-- TODO --",
	}

	// query all visitors for listing
	var visitors []Visitor
	err := db.All(&visitors)
	if err != nil {
		data["ERRORMSG"] = "DB error: " + err.Error()
	}

	data["VISITORS"] = visitors

	fmt.Println("--------------------->", visitors)

	// query all links to see which visitors are allowed for which album
	allowed := map[string]bool{}
	for _, v := range visitors {
		var links []Link
		err = db.Find("VisitorID", v.ID, &links)
		fmt.Println("--------------------->", links)
		if err != nil {
			data["ERRORMSG"] = "DB error: " + err.Error()
		} else {
			for _, l := range links {
				allowed[l.VisitorID] = true
			}
		}
	}
	data["ALLOWED"] = allowed
	fmt.Println("--------------------->", allowed)

	return c.Render(http.StatusOK, "visit.html", &data)
}

func albumsAdd(c echo.Context) error {
	boss, _ := c.Get("ISBOSS").(bool)
	if !boss {
		c.Error(errors.New("You are not the boss."))
		return nil
	}

	aName := c.FormValue("name")
	secTok, err := utils.GenerateSecureKey(16)
	if err != nil {
		c.Error(errors.New("AA 1"))
		return nil
	}

	a := Album{
		Name: aName,
		ID:   secTok,
	}

	err = db.Save(&a)
	if err != nil {
		c.Error(errors.New("AA 2"))
		return nil
	}

	c.Redirect(http.StatusSeeOther, "/")
	return nil
}

func albumsDel(c echo.Context) error {
	boss, _ := c.Get("ISBOSS").(bool)
	if !boss {
		c.Error(errors.New("You are not the boss."))
		return nil
	}

	params, err := c.FormParams()
	if err != nil {
		c.Error(errors.New("AD 1"))
		return nil
	}

	for id, _ := range params {
		a := Album{
			ID: id,
		}
		err := db.DeleteStruct(&a)
		if err != nil {
			c.Error(errors.New("AD 2"))
			return nil
		}
	}

	c.Redirect(http.StatusSeeOther, "/")
	return nil
}

func linkSet(c echo.Context) error {
	return nil
}

func visitorsAdd(c echo.Context) error {
	boss, _ := c.Get("ISBOSS").(bool)
	if !boss {
		c.Error(errors.New("You are not the boss."))
		return nil
	}

	vName := c.FormValue("name")
	secTok, err := utils.GenerateSecureKey(32)
	if err != nil {
		c.Error(errors.New("VA 1"))
		return nil
	}

	v := Visitor{
		Name: vName,
		ID:   secTok,
	}
	err = db.Save(&v)
	if err != nil {
		c.Error(errors.New("VA 2"))
		return nil
	}

	c.Redirect(http.StatusSeeOther, "/")
	return nil
}

func visitorsDel(c echo.Context) error {
	boss, _ := c.Get("ISBOSS").(bool)
	if !boss {
		c.Error(errors.New("You are not the boss."))
		return nil
	}

	params, err := c.FormParams()
	if err != nil {
		c.Error(errors.New("VD 1"))
		return nil
	}

	for id, _ := range params {
		v := Visitor{
			ID: id,
		}
		err := db.DeleteStruct(&v)
		if err != nil {
			c.Error(errors.New("VD 2"))
			return nil
		}
	}

	// TODO -- delete from albums

	c.Redirect(http.StatusSeeOther, "/")
	return nil
}
