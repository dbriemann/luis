package utils

import (
	"crypto/rand"
	"encoding/hex"
)

func GenerateSecureKey(n int) (string, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	if err != nil {
		return "", err
	}

	// return base64.URLEncoding.EncodeToString(b), err
	return hex.EncodeToString(b), err
}
