package main

const (
	BucketVisitors = "Visitors"
)

type Visitor struct {
	ID   string
	Name string `storm:"unique"`
}

type Album struct {
	ID   string
	Name string `storm:"unique"`
}

type Link struct {
	ID        int
	VisitorID string `storm:"index"`
	AlbumID   string `storm:"index"`
}
