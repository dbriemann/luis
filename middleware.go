package main

import (
	"crypto/subtle"

	"github.com/labstack/echo"
)

func Auth(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		c.Set("ISBOSS", false)
		// extract user key from url
		uid := c.QueryParam("v")
		c.Set("USERID", uid)

		// detect if user is bossy
		cookie, err := c.Cookie("sessionkey")
		if err != nil {
			return next(c)
		}

		if subtle.ConstantTimeCompare([]byte(cookie.Value), []byte(sessionKey)) != 1 {
			return next(c)
		}

		// Successfully authed by session key.. proceed.
		c.Set("ISBOSS", true)
		return next(c)
	}
}
